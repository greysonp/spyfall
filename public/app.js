this.app = this.app || {};

(function(module) {

  function init() {
    goToHomeScreen();
  }

  function goToHomeScreen() {
    hideAllScreens();
    showScreen('screen-home');

    document.querySelectorAll('#screen-home button').forEach(function(b) {
      b.onclick = function(e) {
        goToSpySelection(e.target.value);
      };
    });
  }

  function goToSpySelection(playerCount) {
    hideAllScreens();
    showScreen('screen-spies');

    // Add buttons
    var html = '';
    for (var i = 1; i <= playerCount - 1; i++) {
      html += '<button value="' + i + '">' + i + '</button>';
    }
    document.querySelector('#screen-spies .button-grid').innerHTML = html;

    // Add event listeners
    document.querySelectorAll('#screen-spies button').forEach(function(b) {
      b.onclick = function(e) {
        var allSpies = document.getElementById('random-spies').checked;
        var noSpies = document.getElementById('random-no-spies').checked;
        var spyCount = e.target.value;

        if (allSpies && Math.random() < 0.1) {
          spyCount = playerCount;
        } else if (noSpies && Math.random() < 0.1) {
          spyCount = 0;
        }

        goToLocationList(playerCount, spyCount);
      };
    });
  }

  function goToLocationList(playerCount, spyCount) {
    hideAllScreens();
    showScreen('screen-locations');

    // Add list
    var html = '';
    Object.keys(LOCATIONS).forEach(function(location) {
      html += '<div class="checkbox">' + 
              '  <label>' +
              '    <input type="checkbox" value="' + location + '" checked />' +
              '    ' + location +
              '  </label>' +
              '</div>';
    });
    document.querySelector('#screen-locations .location-list').innerHTML = html;

    document.getElementById('begin-button').onclick = function() {
      var elements = document.querySelectorAll('.location-list input:checked');
      var locations = Array.from(elements).map(function(e) {
        return e.value;
      });

      var roles = buildRoles(playerCount, spyCount, chooseRandom(locations));
      goToPlayerReveal(roles, 0);
    };
  }

  function goToPlayerReveal(players, index) {
    hideAllScreens();
    showScreen('screen-reveal');

    document.getElementById('reveal-index').innerText = index + 1;

    document.getElementById('reveal-button').onclick = function() {
      goToPlayerRole(players, index);
    };
  }

  function goToPlayerRole(players, index) {
    hideAllScreens();
    showScreen('screen-role');

    var player = players[index];

    document.getElementById('person-index').innerText = player.index + 1;
    document.getElementById('person-location').innerText = player.location;
    document.getElementById('person-role').innerText = player.role;

    document.getElementById('close-role-button').onclick = function() {
      if (index + 1 >= players.length) {
        goToFinished();
      } else {
        goToPlayerReveal(players, index + 1);
      }
    };
  }

  function goToFinished() {
    hideAllScreens();
    showScreen('screen-finished');

    document.getElementById('restart-button').onclick = function() {
      window.location.reload();
    };
  }

  function buildRoles(playerCount, spyCount, location) {
    console.log('playerCount: ' + playerCount + '  spyCount: ' + spyCount);
    var players = [];
    for (var i = 0; i < playerCount; i++) {
      players.push(i);
    }

    var spies = [];
    for (var i = 0; i < spyCount; i++) {
      spies.push(removeRandom(players));
    }

    var roles = {};

    players.forEach(function(p) {
      roles[p] = {
        'index': p,
        'location': location,
        'role': chooseRandom(LOCATIONS[location]),
      };
    });

    spies.forEach(function(s) {
      roles[s] = {
        'index': s,
        'location': '???',
        'role': 'Spy'
      };
    });

    var out = [];
    for (var i = 0; i < playerCount; i++) {
      out.push(roles[i]);
    }

    console.log(players);
    console.log(spies);
    console.log(roles);
    console.log(out);

    return out;
  }

  function hideAllScreens() {
    var screens = document.getElementsByClassName('screen');

    for (var i = 0; i < screens.length; i++) {
      screens[i].classList.remove('active');
    }
  }

  function showScreen(id) {
    document.getElementById(id).classList.add('active');
  }

  function chooseRandom(list) {
    return list[Math.floor(Math.random() * list.length)];
  }

  function removeRandom(list) {
    var index = Math.floor(Math.random() * list.length);
    return list.splice(index, 1)[0];
  }

  var LOCATIONS = {
    'Airplane': [
      'Pilot',
      'Co-Pilot',
      'Flight Attendant',
      'Passenger'
    ],
    'Arcade': [
      'Pinball Wizard',
      'Donkey Kong High Score Holder',
      'Casual Gamer'
    ],
    'Amusement Park': [
      'Adrenaline Junkee',
      'Weak Stomach'
    ],
    'Auto Repair Shop': [
      'Mechanic',
      'Customer'
    ],
    'Bank': [
      'Teller',
      'Customer'
    ],
    'Beach': [
      'Getting Your Tan On',
      'Casual Volleyball Player',
      'Sand Castle Artist'
    ],
    'Casino': [
      'Blackjack Dealer', 
      'Gambling Addict'
    ],
    'Church': [
      'Priest',
      'Church Member'
    ],
    'Circus': [
      'Trapeze Artist',
      'Lion Tamer',
      'Magician',
      'Audience Member',
      'Ring Leader'
    ],
    'Cruise Ship': [
      'Captain',
      'Passenger'
    ],
    'Day Spa': [
      'Masseuse',
      'Customer'
    ],
    'Grocery Store': [
      'Cashier',
      'Bag Boy',
      'Shopper'
    ],
    'Hospital': [
      'Doctor', 
      'Nurse', 
      'Receptionist'
    ],
    'Hotel': [
      'Receptionist',
      'Visitor'
    ],
    'Military Base': [
      'Sargeant',
      'Foot Solider'
    ],
    'Movie Theater': [
      'Customer',
      'Ticket Cashier',
      'Concessions Cashier'
    ],
    'Night Club': [
      'Wild Child',
      'Wallflower'
    ],
    'Office Party': [
      'Boss',
      'Secretary',
      'Sales Person'
    ],
    'Passenger Train': [
      'Driver',
      'Rider'
    ],
    'Pirate Ship': [
      'Captain',
      'First Mate',
      'Crew Member'
    ],
    'Police Station': [
      'Police Officer',
      'Sheriff',
      'Criminal'
    ],
    'Rock Concert': [
      'Super Fan',
      'Casual Listener'
    ],
    'Sewer': [
      'Rat',
      'Mole Person'
    ],
    'Shopping Mall': [
      'Visitor'
    ],
    'Skate Park': [
      'Pro Skater',
      'Casual Skater'
    ],
    'Space Station': [
      'Biologist',
      'Engineer',
      'Medical Doctor'
    ],
    'Submarine': [
      'Captain',
      'Solider'
    ],
    'University': [
      'Professor',
      'Student'
    ],
    'Zoo': [
      'Zookeeper',
      'Visitor'
    ]
  };

  // Exports
  module.init = init;

})(this.app);
